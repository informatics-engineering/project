# Job Interview 1

### Pilih Aplikasi
> Gmail

### Use Case
Status | Nama Use Case | Nilai Prioritas
--- | --- | --- 
public | Registrasi | 100
user | Login/Logout| 90
user | Mengirim Email| 80
user | Membuka Email| 80
user | Membalas Email| 80
user | Menghapus Email | 80
user | Meneruskan Email| 70
user | Mencari Email| 70
user | Mengarsipkan Email | 50
user | Draft Email | 50
user | Memindahkan Email ke suatu folder| 40
user | Menandai spam Email| 40
user | Switch Akun| 20
user | Pengaturan Email| 20
manajemen | Lihat Daftar Email Masuk | 70
manajemen | Lihat Daftar Akun | 20
manajemen |  Beri Notifikasi | 10 


### Class Diagram
- [Class Diagram ](Class_Diagram.png)

### Coding
- [Source Code](MainGmail.java)

### Video Youtube
- [Link Youtube](https://youtu.be/UjzobDczDrM)

